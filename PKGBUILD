# Maintainer: Muhammed Efe Çetin <efectn@protonmail.com>
# Contributor: Guillaume Benoit <guillaume@manjaro.org>
# Contributor: Philip Müller <philm@manjaro.org>
# Contributor: Helmut Stult <helmut@manjaro.org>

pkgbase=pamac
pkgname=('pamac-cli' 'pamac-gtk' 'pamac-gnome-integration')
_pkgver=10.3.0
pkgver=10.3.0
pkgrel=1
pkgdesc="A Package Manager based on libalpm with AUR and Appstream support"
arch=('x86_64')
url="https://gitlab.manjaro.org/applications/pamac"
license=('GPL3')
depends=('libpamac>=11.2' 'gtk3>=3.24' 'libhandy' 'libnotify' 'libsoup' 'appstream-glib>=0.7.18-1' 'archlinux-appstream-data')

makedepends=('gettext' 'itstool' 'vala>=0.46' 'gtk3>=3.22' 'asciidoc' 'meson' 'ninja' 'gobject-introspection' 'libappindicator-gtk3' 'xorgproto')
options=(!emptydirs !strip)
groups=("pamac")
source=("pamac-$pkgver-$pkgrel.tar.gz::$url/-/archive/v$pkgver/pamac-v$pkgver.tar.gz")
sha256sums=('aa9f2760cf6a08df658067e5c61f62aecb01db3fd32efd8b3a9c268ecad40fdc')

prepare() {
  cd "$srcdir/pamac-v$_pkgver"
  local src
  for src in "${source[@]}"; do
      src="${src%%::*}"
      src="${src##*/}"
      [[ $src = *.patch ]] || continue
      echo "Applying patch $src..."
      patch -Np1 < "../$src"
  done

  # adjust version string
  sed -i -e "s|\"$_pkgver\"|\"$pkgver-$pkgrel\"|g" src/version.vala
}

build() {
  cd "$srcdir/pamac-v$_pkgver"
  mkdir -p builddir
  cd builddir
  meson --prefix=/usr \
        --sysconfdir=/etc \
        -Denable-fake-gnome-software=true \
        --buildtype=release

  # build
  ninja
}

package_pamac-gtk() {
  depends=("pamac-cli=$pkgver-$pkgrel" 'libpamac>=11.1' 'libsoup' 'gtk3>=3.24' 'libhandy' 'archlinux-appstream-data' 'desktop-file-utils' 'libnotify')
  optdepends=('pamac-gnome-integration: for integration into Gnome'
              'libpamac-flatpak-plugin: for Flatpak support'
              'libpamac-snap-plugin: for Snap support'
	            'polkit-gnome: needed for authentification in Cinnamon, Gnome'
              'lxsession: needed for authentification in Xfce, LXDE etc.')
  provides=("pamac-gtk=$pkgver-$pkgrel")
  conflicts=('pamac-aur' 'pamac-gtk-dev')
  install=pamac-gtk.install
  cd "$srcdir/pamac-v$_pkgver"
  cd builddir
  DESTDIR="$pkgdir" ninja install
  # remove pamac-cli
  rm "$pkgdir/usr/bin/pamac"
  rm -rf "$pkgdir/usr/share/man"
  rm -rf "$pkgdir/usr/share/bash-completion"
  rm -rf "$pkgdir/usr/share/fish"
  rm -rf "$pkgdir/usr/share/zsh"
  # remove pamac-gnome-integration
  rm "$pkgdir/usr/bin/gnome-software"
  rm "$pkgdir/usr/share/applications/org.gnome.Software.desktop"
  rm "$pkgdir/usr/share/dbus-1/services/org.gnome.Software.service"
}

package_pamac-cli() {
  depends=('libpamac>=11.1')
  provides=("pamac-cli=$pkgver-$pkgrel")
  conflicts=('pamac-aur' 'pamac-cli-dev')
  cd "$srcdir/pamac-v$_pkgver"
  install -Dm755 "builddir/src/pamac" "$pkgdir/usr/bin/pamac"
  install -Dm644 "builddir/data/doc/pamac.8.gz" "$pkgdir/usr/share/man/man8/pamac.8.gz"
  install -Dm644 "builddir/data/doc/pamac.conf.5.gz" "$pkgdir/usr/share/man/man5/pamac.conf.5.gz"
  install -Dm644 "builddir/data/completion/_pamac" "$pkgdir/usr/share/zsh/site-functions/_pamac"
  install -Dm644 "builddir/data/completion/pamac.fish" "$pkgdir/usr/share/fish/vendor_completions.d/pamac.fish"
  install -Dm644 "builddir/data/completion/pamac" "$pkgdir/usr/share/bash-completion/completions/pamac"
}

package_pamac-gnome-integration() {
  pkgdesc="Pamac gnome integration "
  depends=("pamac-gtk=$pkgver-$pkgrel")
  provides=("pamac-gnome-integration=$pkgver-$pkgrel")
  conflicts=("pamac-gnome-integration-dev" 'gnome-software')
  cd "$srcdir/pamac-v$_pkgver"
  install -Dm644 "data/applications/org.gnome.Software.desktop" "$pkgdir/usr/share/applications/org.gnome.Software.desktop"
  install -Dm644 "builddir/data/dbus/org.gnome.Software.service" "$pkgdir/usr/share/dbus-1/services/org.gnome.Software.service"
  install -Dm755 "builddir/src/gnome-software" "$pkgdir/usr/bin/gnome-software"
}